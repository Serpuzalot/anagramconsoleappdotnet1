using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task1.Anagram;


namespace unitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void getReverseLine_Hello_olleHreturned()
        {
            //arrange
            string word = "Hello";
            string expected = "olleH" ;

            //act
            Anagram anagram = new Task1.Anagram.Anagram();
            string actual = anagram.GetReverseLine(word);

            //assert
            Assert.AreEqual(expected,actual);

        }
        [TestMethod()]
        public void getReverseLine_Hello_World_olleH_dlroWreturned()
        {
            //arrange
            string line = "Hello World";
            string expected =  "olleH dlroW";

            //act
            Anagram anagram = new Task1.Anagram.Anagram();
            string actual = anagram.GetReverseLine(line);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]

        public void getReverseLine_null_errorreturned()
        {
            //arange
            string line = null;
            string expected = null;

            //act
            Anagram anagram = new Task1.Anagram.Anagram();
            string actual = anagram.GetReverseLine(line);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]

        public void getReverseLine_a1bcd_efg1h_d1cba_hgf1e_returned()
        {

            //arange
            string line = "a1bcd efg1h";
            string expected = "d1cba hgf1e";

            //act
            Anagram anagram = new Task1.Anagram.Anagram();
            string actual = anagram.GetReverseLine(line);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]

        public void getReverseLine_space_space_123_123returned()
        {

            //arrange
            string line = "  123";
            string expected = "  123";

            //act
            Anagram anagram = new Task1.Anagram.Anagram();
            string actual = anagram.GetReverseLine(line);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]

        public void getReverseLine_space_space_abc123_space_space_cba123returned()
        {
            //arrange
            string line = "  abc123  ";
            string expected = "  cba123  ";

            //act
            Anagram anagram = new Task1.Anagram.Anagram();
            string actual = anagram.GetReverseLine(line);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]

        public void getReverseLine_space_space_abcd_space_space_zxc_space_space_dcba_space_cxzreturned()
        {
            //arrange
            string line = "  abcd   zxc  ";
            string expected = "  dcba   cxz  ";

            //act
            Anagram anagram = new Task1.Anagram.Anagram();
            string actual = anagram.GetReverseLine(line);

            //assert
            Assert.AreEqual(expected, actual);
        }

    }
}
