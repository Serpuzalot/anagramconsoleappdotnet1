﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Anagram
{
    public class Anagram
    {
        char [] ReturnSwapArrayElements(char[]array,int firstElement,int secondElement)
        {
            char temp = array[secondElement];
            array[secondElement] = array[firstElement];
            array[firstElement] = temp;
            return array;
        }
         string GetReverseWord(string word)
        {
            char[] wordArray = word.ToCharArray();
            string reverseWord = "";
            int counter = 0;
            for (int i = 0; i < (wordArray.Length/2); i++)
            {
                if (Char.IsLetter(wordArray[i]) && Char.IsLetter(wordArray[wordArray.Length - 1 - i]))
                {
                    wordArray = ReturnSwapArrayElements(wordArray,i, wordArray.Length - 1 - i);
                }
                else if (!Char.IsLetter(wordArray[i]) && Char.IsLetter(wordArray[wordArray.Length - 1 - i]))
                {
                    while(!Char.IsLetter(wordArray[i+counter]) && Char.IsLetter(wordArray[wordArray.Length - 1 - i]))
                    {
                        counter++;
                    }
                    if (counter == wordArray.Length / 2)
                    {
                        wordArray = ReturnSwapArrayElements(wordArray, i + counter, wordArray.Length - 1 - i);
                        counter = 0;
                        i = wordArray.Length;
                    }
                    else
                    {
                        wordArray = ReturnSwapArrayElements(wordArray, i + counter, wordArray.Length - 1 - i);
                        counter = 0;
                    }
                  
                }
                else if (Char.IsLetter(wordArray[i]) && !Char.IsLetter(wordArray[wordArray.Length - 1 - i]))
                {
                    
                    while (!Char.IsLetter(wordArray[wordArray.Length - 1 - i - counter]))
                    {
                        counter++;
                    }
                    if (counter == wordArray.Length / 2)
                    {
                        wordArray = ReturnSwapArrayElements(wordArray, i, wordArray.Length - 1 - i - counter);
                        counter = 0;
                        i = wordArray.Length;
                    }
                    else
                    {
                        wordArray = ReturnSwapArrayElements(wordArray, i, wordArray.Length - 1 - i - counter);
                        counter = 0;
                    }
                   
                }
            }
            foreach (var el in wordArray)
            {
                reverseWord += el;
            }
            return reverseWord;
        }
        public string GetReverseLine(string line)
        {
            if (String.IsNullOrEmpty(line))
            {
                return null;
            }
            int start = 0;
            int end = 0;
            bool switcher=false;
            string reverseLine = "";
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i]!=' ')
                {
                    if (!switcher)
                    {
                        start = i;
                        switcher = true;
                    }
                }else if (line[i]==' ' && switcher)
                {
                    end = i ;
                    reverseLine += GetReverseWord(line.Substring(start, end-start));
                    start = 0;
                    end = 0;
                    switcher = false;
                }
                if(i == line.Length-1 && end == 0 && switcher )
                {
                    end = i;
                    reverseLine += GetReverseWord(line.Substring(start, end-start+1));
                }
                if(line[i]==' ')
                {
                    reverseLine += line[i];
                }
            }
            return reverseLine;
        }
    }
}
