﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Anagram
{
    class Program
    {
      
        static void Main(string[] args)
        {
             Console.WriteLine("Enter your line,to reverse it:");
             string line = Console.ReadLine();
             Anagram anagram = new Anagram();
             string reverseLine = anagram.GetReverseLine(line);
             Console.WriteLine(reverseLine);
            
        }
    }
}
